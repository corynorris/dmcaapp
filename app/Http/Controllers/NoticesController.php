<?php namespace App\Http\Controllers;


use App\Http\Requests;
use App\Http\Controllers\Controller;


use App\Http\Requests\PrepareNoticeRequest;
use App\Notice;
use App\Provider;
use Auth;
use Flash;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Mail;

class NoticesController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');

        parent::__construct();
    }

    /**
     * List all notices
     * @return string
     */
    public function index()
    {
        $notices = $this->user->notices()->latest()->get();


        return view('notices.index', compact('notices'));
    }

    /**
     * Create a new notice
     */
    public function create()
    {
        // get list of providers
        $providers = Provider::lists('name', 'id');

        // load a view to create a new notice
        return view('notices.create', compact('providers'));
    }

    /**
     * Ask the user to confirm the DMCA notice that will be delivered
     * @param PrepareNoticeRequest $request
     * @return \Illuminate\View\View
     */
    public function confirm(PrepareNoticeRequest $request)
    {
        $template = $this->compileDmcaTemplate($data = $request->all());

        session()->flash('dmca', $data);

        return view('notices.confirm', compact('template'));

    }


    public function store(Request $request)
    {
        $notice = $this->createNotice($request);

        // Firing an email is one of the slowest things you can do
        // So queuing is standard, and stops the user from having to wait


        Mail::queue(['text' =>'emails.dmca'], compact('notice'), function ($message) use ($notice)
        {
            $message->from($notice->getOwnerEmail())
                    ->to($notice->getRecipientEmail())
                    ->subject('DMCA Notice');

        });

        Flash::success('Your DMCA notice has been delivered');

        return redirect('notices');
    }


    /**
     * Compile a DMCA notice from the form data
     * @param $data
     * @return mixed
     */
    public function compileDmcaTemplate($data)
    {
        $data = $data + [
                'name'  => $this->user->name,
                'email' => $this->user->email,
            ];

        $template = view()->file(app_path('Http/Templates/dmca.blade.php'), $data);

        return $template;
    }

    /**
     * Create and persist a new DMCA notice
     *
     * @param Request $request
     * @return $this
     */
    public function createNotice(Request $request)
    {

        $data = session()->get('dmca');

        $notice = Notice::open($data)
            ->useTemplate($request->input('template'));

        $this->user->notices()->save($notice);

//        $notice = session()->get('dmca') + ['template' => $request->input('template')];
//        $notice = Auth::user()->notices()->create($notice);
        return $notice;
    }


    public function update($noticeId, Request $request)
    {
        $isRemoved = $request->has('content_removed');

        Notice::findOrFail($noticeId)
            ->update(['content_removed' => $isRemoved]);

    }
}