<table class="table table-striped table-bordered">
    <thead>
    <th>This Content</th>
    <th>Accessible Here</th>
    <th>Is Infringing Upon My Work Here</th>
    <th>Notice Sent</th>
    <th>Content Removed</th>
    </thead>

    <tbody>

    @foreach ($notices as $notice)
        <tr>
            <td>{{ $notice->infringing_title }}</td>
            <td>{!! url($notice->infringing_link) !!}</td>
            <td>{!! url($notice->original_link) !!}</td>
            <td>{{ $notice->created_at->diffForHumans() }}</td>
            <td>
                <!-- data-click-submits-form is a custom html5 attribute -->
                {!! Form::open(['data-remote', 'method' => 'PATCH', 'url' => 'notices/' . $notice->id]) !!}
                <div class="form-group">
                    <!-- Content_removed Form Input -->
                    <div class="form-group">
                        {!! Form::checkbox('content_removed', $notice->content_removed, $notice->content_removed, ['data-click-submits-form']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>

</table>