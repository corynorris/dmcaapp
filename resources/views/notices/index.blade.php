@extends('app')

@section('content')
    <h1 class="page-heading">Your Notices</h1>

    <h3>Active</h3>
   @include('notices._table',['notices' => $notices->where('content_removed', 0, false)])

    <h3>Archived</h3>
    @include('notices._table',['notices' => $notices->where('content_removed', 1, false)])

    @unless(count($notices))
        <p class="text-center">You haven't sent any DMCA notices yet.</p>
    @endunless

@stop

