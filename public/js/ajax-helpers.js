/**
 * Created by cory on 04/06/15.
 */

(function() {

    var submitAjaxRequest = function(e) {

        //TIME OUT @ 20 mins
        var form = $(this);
        var method = form.find('input[name="method"]').val() || 'POST';

        $.ajax({
            type: method,
            url: form.prop('action'),
            data: form.serialize(),
            success: function() {
                $.publish('form-submitted', form);
            }
        });

        e.preventDefault(); // disable the form from submitting

    }

    // Forms marked with the "data-remote" attribute will submit via ajax
    $('form[data-remote]').on('submit', submitAjaxRequest);

    // The "data-click-submits-form" attribute immediately submits the form on change
    $('*[data-click-submits-form]').on('change', function() {
        $(this).closest('form').submit();
    });

})();