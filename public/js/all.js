/**
 * Created by cory on 04/06/15.
 */

'use strict';

(function ($) {
    var o = $({});

    $.subscribe = function () {
        o.on.apply(o, arguments);
    };

    $.unsubscribe = function () {
        o.off.apply(o, arguments);
    };

    $.publish = function () {
        o.trigger.apply(o, arguments);
    };
})(jQuery);

/**
 * Created by cory on 04/06/15.
 */

(function () {

    var submitAjaxRequest = function submitAjaxRequest(e) {

        //TIME OUT @ 20 mins
        var form = $(this);
        var method = form.find('input[name="method"]').val() || 'POST';

        $.ajax({
            type: method,
            url: form.prop('action'),
            data: form.serialize(),
            success: function success() {
                $.publish('form-submitted', form);
            }
        });

        e.preventDefault(); // disable the form from submitting
    };

    // Forms marked with the "data-remote" attribute will submit via ajax
    $('form[data-remote]').on('submit', submitAjaxRequest);

    // The "data-click-submits-form" attribute immediately submits the form on change
    $('*[data-click-submits-form]').on('change', function () {
        $(this).closest('form').submit();
    });
})();
/**
 * Created by cory on 04/06/15.
 */

(function () {

    $.subscribe('form-submitted', function () {
        $('.flash').fadeIn(500).delay(1000).fadeOut(500);
    });
})();
//# sourceMappingURL=all.js.map